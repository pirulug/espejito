CREATE TABLE `espejito` (
    /*-- datos --*/
  `id` int(11) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `contra_archivo` varchar(50) NOT NULL,
  `fecha` varchar(10) NOT NULL,
  /*-- enlaces largo--*/
  `enlace0` varchar(500) NOT NULL,
  `enlace1` varchar(500) NOT NULL,
  `enlace2` varchar(500) NOT NULL,
  `enlace3` varchar(500) NOT NULL,
  `enlace4` varchar(500) NOT NULL,
  `enlace5` varchar(500) NOT NULL,
  `enlace6` varchar(500) NOT NULL,
  `enlace7` varchar(500) NOT NULL,
  `enlace8` varchar(500) NOT NULL,
  `enlace9` varchar(500) NOT NULL,
    /*-- enlaces corto--*/
  `corto0` varchar(50) NOT NULL,
  `corto1` varchar(50) NOT NULL,
  `corto2` varchar(50) NOT NULL,
  `corto3` varchar(50) NOT NULL,
  `corto4` varchar(50) NOT NULL,
  `corto5` varchar(50) NOT NULL,
  `corto6` varchar(50) NOT NULL,
  `corto7` varchar(50) NOT NULL,
  `corto8` varchar(50) NOT NULL,
  `corto9` varchar(50) NOT NULL,
    /*-- Nombre de los servidores --*/
  `servidor0` varchar(50) NOT NULL,
  `servidor1` varchar(50) NOT NULL,
  `servidor2` varchar(50) NOT NULL,
  `servidor3` varchar(50) NOT NULL,
  `servidor4` varchar(50) NOT NULL,
  `servidor5` varchar(50) NOT NULL,
  `servidor6` varchar(50) NOT NULL,
  `servidor7` varchar(50) NOT NULL,
  `servidor8` varchar(50) NOT NULL,
  `servidor9` varchar(50) NOT NULL,
    /*-- Logo de los servidores --*/
  `slogo0` varchar(50) NOT NULL,
  `slogo1` varchar(50) NOT NULL,
  `slogo2` varchar(50) NOT NULL,
  `slogo3` varchar(50) NOT NULL,
  `slogo4` varchar(50) NOT NULL,
  `slogo5` varchar(50) NOT NULL,
  `slogo6` varchar(50) NOT NULL,
  `slogo7` varchar(50) NOT NULL,
  `slogo8` varchar(50) NOT NULL,
  `slogo9` varchar(50) NOT NULL,

  `visitas` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `usuario` (
  `user_id` int(11) NOT NULL,
  `user_user` varchar(20) NOT NULL,
  `user_contra` varchar(100) NOT NULL,
  `user_email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `usuario` (`user_id`, `user_user`, `user_contra`, `user_email`) VALUES
(1, 'pirulug', '76749024', 'pirulug@gmail.com');


ALTER TABLE `espejito`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `usuario`
  ADD PRIMARY KEY (`user_id`);