<?php
include "config.php";

if (isset($_GET['v'])) {

    $id = $_GET['v'];

    //verificar si existe o no
    $sqlVerificar = "SELECT * FROM mirror_texto WHERE id LIKE $id";
    $resultadoVerificar = mysqli_query($mysqli, $sqlVerificar);
    $verificar = mysqli_fetch_assoc($resultadoVerificar);

    if(isset($verificar['id'])){

        //conator de visitas
        $sqlVisitas = "SELECT * FROM mirror_visitas WHERE id=$id";
        $resultadoVisitas = mysqli_query($mysqli, $sqlVisitas);
        $visitas = mysqli_fetch_assoc($resultadoVisitas);

        $views = $visitas['visitas']+1;

        $sqlVisitas = mysqli_query($mysqli, "UPDATE mirror_visitas SET visitas='$views' WHERE id=$id");

        //traer url corta
        $sqlCorto = "SELECT * FROM mirror_corto WHERE id=$id";
        $resultadoCorto = mysqli_query($mysqli, $sqlCorto);
        $corto = mysqli_fetch_assoc($resultadoCorto);

        //extraer  imagen
        $sqlSimg = "SELECT * FROM mirror_simg WHERE id=$id";
        $resultadoSimg = mysqli_query($mysqli, $sqlSimg);
        $simg = mysqli_fetch_assoc($resultadoSimg);

        //Traer  Nombre del servidor
        $sqlNservidor = "SELECT * FROM mirror_servidor WHERE id=$id";
        $resultadoNservidor = mysqli_query($mysqli, $sqlNservidor);
        $servidor = mysqli_fetch_assoc($resultadoNservidor);

        //Traer  los datos
        $sqlDatos = "SELECT * FROM mirror_texto WHERE id=$id";
        $resultadoDservidor = mysqli_query($mysqli, $sqlDatos);
        while ($fila = mysqli_fetch_array($resultadoDservidor)) {
            $id = $fila['id'];
            $dtitulo = $fila['titulo'];
            $pass_archivo = $fila['pass_archivo'];
            $fecha = $fila['fecha'];
        }
        $fecha = explode("-", $fecha);

        $fecha = $fecha['2']."/".$fecha['1']."/".$fecha['0'];
    
?>
        <!DOCTYPE html>
        <html lang="es">
            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Document</title>

                <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

                <!-- Fontawesome -->
                <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css" integrity="sha384-vp86vTRFVJgpjF9jiIGPEEqYqlDwgyBgEF109VFjmqGmIY/Y4HV4d3Gp2irVfcrp" crossorigin="anonymous">


                <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
                <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

            </head>
            <body>
            <div class="container">

                <div class="card">
                    <div class="card-body text-center">
                        <h6>Has solicitado el siguiente archivo</h6>
                        <h3><?= $dtitulo ?></h3>
                        <p>
                            <b>Fecha de subida :</b> <?= $fecha ?><br>
                            <b>Número de vistas :</b> <?= $visitas['visitas'] ?><br>
                            <b>Contraseña del archivo :</b> <?= $pass_archivo ?>
                        </p>
                    </div>
                </div>

                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Host</th>
                            <th>Enlace del Archivo</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php if($corto['corto0'] != ""){ ?>
                            <tr>
                                <td><img src="<?= $simg['simg0'] ?>" title="<?= $servidor['servidor0'] ?>"></td>
                                <td><a href="<?= $corto['corto0'] ?>" target="_blank" class="btn btn-success"><i class="fa fa-download"></i>  Obtener Enlace</a></td>
                            </tr>
                        <?php  }  ?>

                        <?php if($corto['corto1'] != ""){ ?>
                            <tr>
                                <td><img src="<?= $simg['simg1'] ?>" title="<?= $servidor['servidor1'] ?>"></td>
                                <td><a href="<?= $corto['corto1'] ?>" target="_blank" class="btn btn-success"><i class="fa fa-download"></i>  Obtener Enlace</a></td>
                            </tr>
                        <?php  }  ?>

                        <?php if($corto['corto2'] != ""){ ?>
                            <tr>
                                <td><img src="<?= $simg['simg2'] ?>" title="<?= $servidor['servidor2'] ?>"></td>
                                <td><a href="<?= $corto['corto2'] ?>" target="_blank" class="btn btn-success"><i class="fa fa-download"></i>  Obtener Enlace</a></td>
                            </tr>
                        <?php  }  ?>

                        <?php if($corto['corto3'] != ""){ ?>
                            <tr>
                                <td><img src="<?= $simg['simg3'] ?>" title="<?= $servidor['servidor3'] ?>"></td>
                                <td><a href="<?= $corto['corto3'] ?>" target="_blank" class="btn btn-success"><i class="fa fa-download"></i>  Obtener Enlace</a></td>
                            </tr>
                        <?php  }  ?>

                        <?php if($corto['corto4'] != ""){ ?>
                            <tr>
                                <td><img src="<?= $simg['simg4'] ?>" title="<?= $servidor['servidor4'] ?>"></td>
                                <td><a href="<?= $corto['corto4'] ?>" target="_blank" class="btn btn-success"><i class="fa fa-download"></i>  Obtener Enlace</a></td>
                            </tr>
                        <?php  }  ?>

                        <?php if($corto['corto5'] != ""){ ?>
                            <tr>
                                <td><img src="<?= $simg['simg5'] ?>" title="<?= $servidor['servidor5'] ?>"></td>
                                <td><a href="<?= $corto['corto5'] ?>" target="_blank" class="btn btn-success"><i class="fa fa-download"></i>  Obtener Enlace</a></td>
                            </tr>
                        <?php  }  ?>

                        <?php if($corto['corto6'] != ""){ ?>
                            <tr>
                                <td><img src="<?= $simg['simg6'] ?>" title="<?= $servidor['servidor6'] ?>"></td>
                                <td><a href="<?= $corto['corto6'] ?>" target="_blank" class="btn btn-success"><i class="fa fa-download"></i>  Obtener Enlace</a></td>
                            </tr>
                        <?php  }  ?>

                        <?php if($corto['corto7'] != ""){ ?>
                            <tr>
                                <td><img src="<?= $simg['simg7'] ?>" title="<?= $servidor['servidor7'] ?>"></td>
                                <td><a href="<?= $corto['corto7'] ?>" target="_blank" class="btn btn-success"><i class="fa fa-download"></i>  Obtener Enlace</a></td>
                            </tr>
                        <?php  }  ?>

                        <?php if($corto['corto8'] != ""){ ?>
                            <tr>
                                <td><img src="<?= $simg['simg8'] ?>" title="<?= $servidor['servidor8'] ?>"></td>
                                <td><a href="<?= $corto['corto8'] ?>" target="_blank" class="btn btn-success"><i class="fa fa-download"></i>  Obtener Enlace</a></td>
                            </tr>
                        <?php  }  ?>

                        <?php if($corto['corto9'] != ""){ ?>
                            <tr>
                                <td><img src="<?= $simg['simg9'] ?>" title="<?= $servidor['servidor9'] ?>"></td>
                                <td><a href="<?= $corto['corto9'] ?>" target="_blank" class="btn btn-success"><i class="fa fa-download"></i>  Obtener Enlace</a></td>
                            </tr>
                        <?php  }  ?>

                    </tbody>
                </table>
            </div>

            </body>
        </html>

<?php
    }else{
        echo "<script>location.href ='404.html';</script>";
    }
}else{
    echo "<script>location.href ='404.html';</script>";
}