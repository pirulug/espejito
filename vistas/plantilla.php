<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <title>Sales Admin | CORK - Multipurpose Bootstrap Dashboard Template </title>
    <link rel="icon" type="image/x-icon" href="vistas/assets/img/favicon.ico"/>
    <link href="vistas/assets/css/loader.css" rel="stylesheet" type="text/css" />
    <script src="vistas/assets/js/loader.js"></script>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
    <link href="vistas/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="vistas/assets/css/plugins.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM STYLES -->
    <link href="vistas/plugins/apex/apexcharts.css" rel="stylesheet" type="text/css">
    <link href="vistas/assets/css/dashboard/dash_1.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS/CUSTOM STYLES -->

</head>
<body>
    <!-- BEGIN LOADER -->
    <div id="load_screen"> <div class="loader"> <div class="loader-content">
        <div class="spinner-grow align-self-center"></div>
    </div></div></div>
    <!--  END LOADER -->

    <!-- top memu -->
    <?php include "vistas/inc/topMenu.php" ?>
    <!-- /topmenu -->
    

    <!--  BEGIN MAIN CONTAINER  -->
    <div class="main-container" id="container">

        <div class="overlay"></div>
        <div class="search-overlay"></div>

        <!--  BEGIN SIDEBAR  -->
        
        <!--  END SIDEBAR  -->
        
        <!--  BEGIN CONTENT AREA  -->
        
        <!--  END CONTENT AREA  -->

    </div>
    <!-- END MAIN CONTAINER -->

    <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
    <script src="vistas/assets/js/libs/jquery-3.1.1.min.js"></script>
    <script src="vistas/bootstrap/js/popper.min.js"></script>
    <script src="vistas/bootstrap/js/bootstrap.min.js"></script>
    <script src="vistas/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="vistas/assets/js/app.js"></script>
    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
    <script src="vistas/assets/js/custom.js"></script>
    <!-- END GLOBAL MANDATORY SCRIPTS -->

    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->
    <script src="vistas/plugins/apex/apexcharts.min.js"></script>
    <script src="vistas/assets/js/dashboard/dash_1.js"></script>
    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->

</body>
</html>